import React, { useRef, useEffect, useState, useCallback } from 'react'
import styled, { createGlobalStyle } from 'styled-components'

export const GlobalStyle = createGlobalStyle`
  body,ul,div {
    margin: 0;
    padding: 0;
    box-sizing: border-box;
  }
  a {
    text-decoration: none
  }
  ol, ul {
    list-style: none;
  }
`

const Container = styled.div`
  width: 4rem;
  height: 2rem;
  min-width: 10rem;
  cursor: pointer;
`
const ComboboxMain = styled.div`
  width: 100%;
  height: 100%;
  background-color: yellow;
  position: relative;
  padding-left: 1rem;
  &:after {
    border-right: 1px solid black;
    border-bottom: 1px solid black;
    width: 0.8rem;
    height: 0.8rem;
    position: absolute;
    top: 0;
    right: 0;
    transform: ${(props) =>
      props.isArrowUp
        ? 'translate(-65%, 80%) rotate(225deg)'
        : 'translate(-65%, 40%) rotate(45deg)'};
    content: ' ';
  }
`
const ComboboxList = styled.ul`
  background-color: orange;
  border: 1px solid grey;
  height: 5rem;
  min-width: 10rem;
  overflow-y: auto;
  overflow-x: hidden;
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  align-items: flex-start;
  visibility: ${(props) => (props.isShown ? 'visible' : 'collapse')};
`
const ComboboxItem = styled.li`
  background-color: ${(props) => (props.selected ? 'yellowgreen' : 'green')};
  padding: 0.2rem 1rem;
  width: 100%;
  &:hover {
    background-color: red;
    color: white;
  }
`
const Combobox = () => {
  const list = ['1', '2', '3', '4', '5']
  const [isArrowUp, setArrowUp] = useState(false)
  const [selectedItem, setSelectedItem] = useState(list[0])
  const [isComboListShow, setComboListShow] = useState(false)
  const main = useRef()
  const comboList = useRef()

  const clickCombo = useCallback((e) => {
    setArrowUp((prev) => !prev)
    setComboListShow((prev) => !prev)
  }, [])

  const clickItem = useCallback((e) => {
    if (e.target.closest('li')) {
      console.log('click', e)
      setSelectedItem(e.target.textContent)
      setArrowUp((prev) => !prev)
      setComboListShow((prev) => !prev)
    }
  }, [])

  const keyDownHandler = useCallback((e) => {
    e.preventDefault()
    console.log('keydown', e)
    if (comboList.current) {
      const t = comboList.current.querySelector('.selected')
      if (e.key === 'ArrowDown' && t.nextElementSibling) {
        setSelectedItem(t.nextElementSibling.textContent)
        t.nextElementSibling.scrollIntoView()
      } else if (e.key === 'ArrowUp' && t.previousElementSibling) {
        setSelectedItem(t.previousElementSibling.textContent)
        t.previousElementSibling.scrollIntoView()
      } else if (e.key === 'Enter') {
        setArrowUp((prev) => !prev)
        setComboListShow((prev) => !prev)
      }
    }
  }, [])

  useEffect(() => {
    if (isComboListShow) {
      comboList.current.focus()
      console.log(comboList.current)
      const t = comboList.current.querySelector('.selected')
      if (t) {
        t.focus()
        t.scrollIntoView()
      }
    }
  }, [isComboListShow])

  return (
    <>
      <Container>
        <ComboboxMain
          ref={main}
          isArrowUp={isArrowUp}
          content={selectedItem}
          onClick={clickCombo}
        >
          {selectedItem}
        </ComboboxMain>
        <ComboboxList
          isShown={isComboListShow}
          ref={comboList}
          onClick={clickItem}
          onKeyDown={keyDownHandler}
          tabIndex="0"
        >
          {list.map((item, i) => (
            <ComboboxItem
              selected={item === selectedItem}
              className={item === selectedItem ? 'selected' : 'unselected'}
              key={i}
            >
              {item}
            </ComboboxItem>
          ))}
        </ComboboxList>
      </Container>
      <GlobalStyle />
    </>
  )
}

export default Combobox
