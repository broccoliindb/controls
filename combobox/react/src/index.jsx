import React from 'react'
import ReactDom from 'react-dom'
import Combobox from './Combobox'

ReactDom.render(<Combobox />, document.querySelector('#root'))
